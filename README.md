Android 程序设计课程项目
===================
2020 秋季学期 教师 简星


第二周 布局管理器 LinearLayout
--------------------------------

#### 作业题目

请使用 LinearLayout 布局管理器按下图完成一个登录界面的设计<br>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0916/143048_f8898b85_7835109.png "LinearLayoutLogin.png")

#### 作业说明
请使用：线性布局LinearLayout，文本TextView，输入框EditText，按钮Button等组件完成以上作业。

#### 作业要求：
使用 Eclipse 工具；独立完成；包名前缀为 edu.cque.yourname

#### 完成时间：
一周